// This is Just a Meeting place for all the reducers we have in the App
import { combineReducers } from 'redux'

// Reducers Imports
import itemReducer from './itemReducer';
import authReducer from './authReducer';
import errorReducer from './errorReducer';

export default combineReducers({
    item: itemReducer,
    auth: authReducer,
    error: errorReducer,
})