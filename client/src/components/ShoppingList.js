import React, { Component } from 'react'
import PropTypes from 'prop-types'

// React-Strap Imports
import {
    Button,
    ListGroup,
    Container,
    ListGroupItem
} from 'reactstrap'

// React-transition group
import { CSSTransition, TransitionGroup } from 'react-transition-group'

// Redux Imports
import { connect } from 'react-redux'
import { getItems, deleteItem } from '../actions/itemActions'

class ShoppingList extends Component {

    componentDidMount() {
        this.props.getItems()
    }

    static propTypes = {
        items: PropTypes.array.isRequired,
        getItems: PropTypes.func.isRequired,
        deleteItem: PropTypes.func.isRequired,
        isAuthenticated: PropTypes.bool
    }

    // Handle delete Item
    onDeleteClick = (itemId) => {
        this.props.deleteItem(itemId)
    }

    render() {
        const { items } = this.props

        return (
            <Container>
                <ListGroup>
                    <TransitionGroup className="shopping-list">
                        {
                            items.map(({ _id, name }) => (
                                <CSSTransition key={_id} timeout={500} classNames="fade">
                                    <ListGroupItem>
                                        {
                                            this.props.isAuthenticated ?
                                                <Button
                                                    className="remove-btn"
                                                    color="danger"
                                                    size="sm"
                                                    onClick={this.onDeleteClick.bind(this, _id)}
                                                >
                                                    &times;
                                                </Button> :
                                                null
                                        }
                                        {name}
                                    </ListGroupItem>
                                </CSSTransition>
                            ))
                        }
                    </TransitionGroup>
                </ListGroup>
            </Container>
        )
    }
}



const mapStateToProps = (state) => ({
    items: state.item.items,
    isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps, { getItems, deleteItem })(ShoppingList)
