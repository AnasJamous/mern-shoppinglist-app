import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'

// Redux Imports
import { connect } from 'react-redux'
import { logout } from '../../actions/authActions'

// Reactstrap Imports
import { NavLink } from 'reactstrap'

export class Logout extends Component {

    static propTypes = {
        logout: PropTypes.func.isRequired
    }

    render() {
        return (
            <Fragment>
                <NavLink onClick={this.props.logout} href="#">
                    Logout
                </NavLink>
            </Fragment>
        )
    }
}

export default connect(null, { logout })(Logout)
