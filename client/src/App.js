import React, { Component } from 'react';
import './App.css';

// Redux Imports
import { Provider } from 'react-redux'
import store from './store';
import { loadUser } from './actions/authActions'

// Reactstrap Imports
import { Container } from 'reactstrap'

// Bootstrap Import
import 'bootstrap/dist/css/bootstrap.min.css'

// Components Import
import AppNavbar from './components/AppNavbar'
import ShoppingList from './components/ShoppingList'
import ItemModal from './components/ItemModal'

//

class App extends Component {

  componentDidMount() {
    store.dispatch(loadUser())
  }

  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <AppNavbar />
          <Container>
            <ItemModal />
            <ShoppingList />
          </Container>
        </div>
      </Provider>
    );
  }
}

export default App;
