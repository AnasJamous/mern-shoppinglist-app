import axios from 'axios'
import { returnErrors } from './errorActions'

// Types Imports
import {
    USER_LOADED,
    USER_LOADING,
    LOGIN_FAIL,
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS,
    REGISTER_FAIL,
    REGISTER_SUCCESS,
    AUTH_ERROR
} from './types'

// Check token & load user
export const loadUser = () => (dispatch, getState) => {
    // User Loading
    dispatch({ type: USER_LOADING });


    axios.get('/api/auth/user', tokenConfig(getState))
        .then(res =>
            dispatch({
                type: USER_LOADED,
                payload: res.data // will return the user obj
            })
        )
        .catch(e => {
            dispatch(returnErrors(e.response.data, e.response.status))
            dispatch({
                type: AUTH_ERROR
            })
        })
}

//Register user
export const register = ({ name, email, password }) => dispatch => {
    // Headers
    const config = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    // Request body
    const body = JSON.stringify({ name, email, password }) // turning on javaScript obj to json

    axios.post('/api/users', body, config)
        .then(res => {
            dispatch({
                type: REGISTER_SUCCESS,
                payload: res.data
            })
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status, 'REGISTER_FAIL'))
            dispatch({
                type: REGISTER_FAIL
            })

        })
}

// Login User
//Register user
export const login = ({ email, password }) => dispatch => {
    // Headers
    const config = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    // Request body
    const body = JSON.stringify({ email, password }) // turning on javaScript obj to json

    axios.post('/api/auth', body, config)
        .then(res => {
            dispatch({
                type: LOGIN_SUCCESS,
                payload: res.data
            })
        })
        .catch(err => {
            dispatch(returnErrors(err.response.data, err.response.status, 'LOGIN_FAIL'))
            dispatch({
                type: LOGIN_FAIL
            })

        })
}

//Logout User
export const logout = () => {
    return {
        type: LOGOUT_SUCCESS
    }
}


// Setup Config/ headers & token
export const tokenConfig = getState => {
    // Get token from localStorage
    const token = getState().auth.token;

    // Set token to headers
    const config = {
        headers: {
            "Content-Type": "application/json"
        }
    }

    // if token, add to headers
    if (token) {
        config.headers['x-auth-token'] = token
    }

    return config
}
