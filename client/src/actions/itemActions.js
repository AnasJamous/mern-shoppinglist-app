import axois from 'axios'
import {
    GET_ITEMS,
    ADD_ITEM,
    DELETE_ITEM,
    ITEMS_LOADING
} from './types';

import { tokenConfig } from './authActions'
import { returnErrors } from './errorActions'


// Get Items
export const getItems = () => dispatch => {
    // Calling setItemLoading
    dispatch(setItemsLoading());

    axois
        .get('/api/items')
        .then(res =>
            dispatch({
                type: GET_ITEMS,
                payload: res.data
            })
        )
}

// Delete Item
export const deleteItem = id => (dispatch, getState) => {
    axois.delete(`/api/items/${id}`, tokenConfig(getState))
        .then(() =>
            dispatch({
                type: DELETE_ITEM,
                payload: id
            })
        )
        .catch(err =>
            dispatch(
                returnErrors(
                    err.response.data,
                    err.response.status
                ))
        )
}

// Add Item
export const addItem = newItem => (dispatch, getState) => {
    axois.post('/api/items', newItem, tokenConfig(getState))
        .then(res =>
            dispatch({
                type: ADD_ITEM,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch(
                returnErrors(
                    err.response.data,
                    err.response.status
                ))
        )
}

// Set Item Loading
export const setItemsLoading = () => {
    return {
        type: ITEMS_LOADING
    }
}